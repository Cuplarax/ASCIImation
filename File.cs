﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Drawing;

public static class FileWork
{
	public static string[] Files;
	public static bool Selected = false;
	public static void Error(string error_text)
	{
		Console.Clear();
		Selected = false;
		Misc.Color.Print("произошла ошибка:", ConsoleColor.Red);
		Misc.Color.Print("|\n+- " + error_text, ConsoleColor.Red);
		ConsoleWork.Pause();
		
	}
	private static int Separate(string raw, string name, string ext) => Int32.Parse(raw.Replace(name, "").Replace('.' + ext, ""));
	public static void GetBmpFiles(string path)
	{
		string[] Allfiles;
		List<string> bmpFiles = new List<string> { };
		int start;
		Selected = true;
		try
		{
			FileAttributes attr = File.GetAttributes(path);
			if (attr.HasFlag(FileAttributes.Directory)) { }
			else Error("Указанный путь является файлом, а не директорией.");
		}
		catch (Exception)
		{
			Error("Указанного пути не существует.");
		}
		if (!Selected) return;
		Directory.GetFiles(path);
		ShowProcess.Header();
		// получение всех файлов
		ShowProcess.BeforeProcess("получение всех файлов в директории", "сбор");
		Allfiles = Directory.GetFiles(path);
		for (int f = 0; f < Allfiles.Length; f++)
			Allfiles[f] = Path.GetFileName(Allfiles[f]);
		ShowProcess.AfterProcess($"получено файлов: {Allfiles.Length}", "получено");
		// получение бмп файлов
		ShowProcess.BeforeProcess("фильтрация файлов по расширению", "фильтрация");
		foreach (string file in Allfiles)
			if (Path.GetExtension(file) == ".bmp")
				bmpFiles.Add(file);
		Allfiles = bmpFiles.ToArray();
		bmpFiles.Clear();
		ShowProcess.AfterProcess($"получено bmp-файлов: {Allfiles.Length}", "отфильтровано");
		// получение кадров
		ShowProcess.BeforeProcess("фильтрация файлов по имени", "фильтрация");
		foreach (string filename in Allfiles)
			if (Int32.TryParse(filename.Replace("ASCIIframe", "").Replace(".bmp", ""), out start))
				bmpFiles.Add(filename);
		ShowProcess.AfterProcess($"получено кадров: {bmpFiles.Count}", "отфильтровано");
		// сортировка кадров
		ShowProcess.BeforeProcess("сортировка кадров по порядку", "сортировка");
		string buffer;
		start = 0;
		bool done = false;
		while (!done)
		{
			done = true;
			for (int k = start; k < bmpFiles.Count; k++)
				if (k < bmpFiles.Count - 1 && Separate(bmpFiles[k], "ASCIIframe", "bmp") > Separate(bmpFiles[k + 1], "ASCIIframe", "bmp"))
				{
					buffer = bmpFiles[k + 1];
					bmpFiles[k + 1] = bmpFiles[k];
					bmpFiles[k] = buffer;
					done = false;
				}
				else if (done && k > 0)
					start = k - 1;
		}
		Files = bmpFiles.ToArray();
		ShowProcess.AfterProcess("сортировка кадров по порядку", "отсортировано");
		//
		GetFrames();
		ConsoleWork.Pause();
	}
	public static void GetFrames()
	{
		Bitmap bmp;
		Image img;
		int w = 0;
		int h = 0;
		int num = 0;
		List<List<Color>> Frame = new List<List<Color>> { };
		bool first = true;
		foreach (string l in Files)
		{
			ConsoleWork.ClearLine();
			ShowProcess.BeforeProcess("получение содержимого кадров", $"{num + 1}\\{Files.Length}", ConsoleColor.Yellow, ConsoleColor.Yellow);
			img = Image.FromFile(Setup.Path + "\\" + l);
			bmp = new Bitmap(Setup.Path + "\\" + l);
			if (first)
			{
				w = img.Size.Width;
				h = img.Size.Height;
				if (w > 120 || h > 30)
				{
					Error($"{l} | кадр не может быть больше 120 * 80 пикселей.");
					return;
				}
				first = false;
			}
			else
			{
				if (img.Size.Width != w || img.Size.Height != h)
				{
					Error($"{l} | кадр не совпадает по размеру с первым кадром");
					return;
				}
			}
			Frame.Add(new List<Color> { });
			for (int i = 0; i < h; i++)
				for (int j = 0; j < w; j++)
					Frame[num].Add(bmp.GetPixel(j, i));
			num++;
		}
		ShowProcess.AfterProcess("получение содержимого кадров", "получено");
		ShowProcess.BeforeProcess("преобразование", "подготовка", ConsoleColor.Yellow, ConsoleColor.Yellow);
		num = 0;
		Animation.Frames.Clear();
		foreach (List<Color> frame in Frame)
		{
			Animation.Frames.Add(new List<Pixel> { });
			ConsoleWork.ClearLine();
			ShowProcess.BeforeProcess("преобразование", $"{num + 1}\\{Frame.Count}", ConsoleColor.Yellow, ConsoleColor.Yellow);
			foreach (Color element in frame)
				Animation.Frames[num].Add(new Pixel(Colors.RGBtoConsoleColor(element)));
			num++;
		}
		Animation.FrameWidth = w;
		Animation.FrameHeight = h;
		ShowProcess.AfterProcess("преобразование", "завершено");
	}
	public static class ShowProcess
	{
		public static int space = 40;
		public static void BeforeProcess(string first_text, string second_text, ConsoleColor first_color = ConsoleColor.White, ConsoleColor second_color = ConsoleColor.Yellow)
		{
			Misc.Color.Print(first_text, first_color, false);
			ConsoleWork.X(space);
			Console.Write('|');
			Console.CursorLeft += 2;
			Misc.Color.Print(second_text, second_color, false);
		}
		public static void AfterProcess(string first_text, string second_text, ConsoleColor first_color = ConsoleColor.Green, ConsoleColor second_color = ConsoleColor.Green)
		{
			ConsoleWork.ClearLine();
			Misc.Color.Print(first_text, first_color, false);
			ConsoleWork.X(space);
			Console.Write('|');
			Console.CursorLeft += 2;
			Misc.Color.Print(second_text, second_color);
		}
		public static void Header()
		{
			Console.CursorTop = 0;
			Console.CursorLeft = 15;
			Console.Write("процесс");
			ConsoleWork.X(space);
			Console.Write('|');
			Console.CursorLeft += 11;
			Console.WriteLine("статус");
		}
	}
}