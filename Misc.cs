﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Drawing;

public static class Misc
{
	public const string decorators = "AdOL[{#@*&=+HBcR";
	private static Colors colors = new Colors(ConsoleColor.Black);
	public static class Color
	{
		public static void Print(string text, ConsoleColor color, bool line = true, bool reset = true)
		{
			Console.ForegroundColor = color;
			Console.Write(text);
			if (line) Console.WriteLine();
			if (reset) Console.ResetColor();
		}
		public static void PrintBg(string text, ConsoleColor color, bool line = true, bool reset = true)
		{
			Console.BackgroundColor = color;
			Console.Write(text);
			if (line) Console.WriteLine();
			if (reset) Console.ResetColor();
		}
	}
	public static bool Choice(ref string start_path)
	{
		var path = new List<char> { };  // Пустой список символов, который будет указывать на папку.
		var key = new ConsoleKeyInfo();  // переменная, которая будет хранить нажатую клавишу.
		foreach (char s in start_path)  // Преобразование строки start_path в набор символов.
			path.Add(s);                //
		Color.Print("Введите расположение bmp-файлов для анимации", ConsoleColor.Cyan);
		Color.Print("Названия bmp-файлов должны выглядеть так: ASCIIframe   .bmp", ConsoleColor.Cyan, false);
		Console.CursorLeft -= 4 + 3;
		Color.Print("[x]", ConsoleColor.Yellow, false);
		Console.CursorLeft += 5;
		Console.WriteLine();
		Color.Print("[x] - номер кадра (целочисленный)", ConsoleColor.Yellow);
		Console.WriteLine();
		Color.Print("ESC - отмена", ConsoleColor.Gray);
		Color.Print("ENTER - подтвердить", ConsoleColor.Gray);
		Console.CursorTop -= 3;
		Console.ForegroundColor = ConsoleColor.White;
		Console.CursorLeft = 0;
		Console.Write(start_path);
		Console.CursorLeft = 0;
		while (true)
		{
			key = Console.ReadKey(true);  // Считываение клавиши без её отображения в консоли.
			if (key.Key == ConsoleKey.Backspace && path.Count > 0)  //? при нажатии backspace:
				path.RemoveAt(path.Count - 1);  // Удаление последнего символа.
			else if (key.Key == ConsoleKey.Enter)  //? при нажатии enter:
			{
				Console.Clear();
				string done = "";  // Пустая строка.
				foreach (char c in path)  // Преобразование набора символов path в строку.
					done += c;            //
				start_path = done;  // Возвращение полученой строки.
				return true;
			}
			else if (key.Key == ConsoleKey.Escape)  // при нажатие escape:
			{
				Console.Clear();
				return false;  // Возвращение кода отмены.
			}
			else if (key.KeyChar != '\n' && key.KeyChar != '\0' && path.Count < 65)  // при нажатии любой другой клавиши, которая не вызывает переноса на строку:
				path.Add(key.KeyChar);  // Добавление введёного символа.
			Console.CursorLeft = 0;
			Console.Write(new String(' ', 65));
			Console.CursorLeft = 0;
			foreach (char c in path)  // Посимвольный вывод введёного текста.
				Console.Write(c);     //
		}
	}
	public static void SetDelay(ref int delay)
	{
		int x = 0;
		int buffer = delay;
		string[] chars = new string[] { "-", "\\", "|", "/" };
		int animation_step = 0;
		int step = 5;
		const int max_cooldown = 6;
		int cooldown = max_cooldown;
		Misc.Color.Print("\nИзменение задержки между кадрами...", ConsoleColor.Cyan, false);
		x = Console.CursorLeft + 1;
		Console.WriteLine();
		Misc.Color.Print($"задержка: {delay}мс.", ConsoleColor.Cyan);
		Misc.Color.Print($"шаг изменения: {step}мс.", ConsoleColor.Blue);
		Misc.Color.Print("ENTER - подтвердить\n" +
			"ESC - отмена\n" +
			"↑ - увеличить шаг\n" +
			"↓ - уменьшить шаг\n" +
			"← - уменьшить задержку на шаг\n" +
			"→ - увеличить задержку на шаг", ConsoleColor.Gray, false);
		Console.CursorTop = 1;
		ConsoleKey key;
		while (true)
		{
			Console.CursorLeft = x;
			Misc.Color.Print(chars[animation_step], ConsoleColor.Cyan, false);
			if (cooldown <= 0)
			{
				animation_step = animation_step <= 2 ? animation_step + 1 : 0;
				cooldown = max_cooldown;
			}
			else cooldown--;
			if (Console.KeyAvailable)
			{
				key = Console.ReadKey(true).Key;
				switch (key)
				{
					case ConsoleKey.UpArrow:
					case ConsoleKey.W:
						if (step < 10)
							step++;
						break;
					case ConsoleKey.DownArrow:
					case ConsoleKey.S:
						if (step > 1)
							step--;
						break;
					case ConsoleKey.RightArrow:
					case ConsoleKey.D:
						if (delay <= 1000)
							delay += step;
						break;
					case ConsoleKey.LeftArrow:
					case ConsoleKey.A:
						if (delay > 5)
							delay -= step;
						break;
					case ConsoleKey.Enter:
						Console.Clear();
						return;
					case ConsoleKey.Escape:
						Console.Clear();
						delay = buffer;
						return;
				}
				if (delay > 1000) delay = 1000;
				if (delay < 5) delay = 5;
				Console.CursorTop = 2;
				Console.CursorLeft = 0;
				Misc.Color.Print($"задержка: {delay}мс.", ConsoleColor.Cyan);
				Misc.Color.Print($"шаг изменения: {step}мс.", ConsoleColor.Blue);
				Console.CursorTop = 1;
				Console.CursorLeft = x;
			}
			Thread.Sleep(20);
		}
	}
	public static int Main_Menu(int symb_top = 6, bool escape = true, params string[] variants)
	{
		ConsoleKeyInfo answer;
		int variant_num = 0;
		foreach (string variant in variants)
		{
			variant_num++;
			Console.WriteLine($"{variant_num}. {variant}");
		}
		var random = new Random();
		int cooldown = 0;
		int symb_pos_x = -1;
		ConsoleColor symb_color = colors.GetRandom;
		char symb = decorators[random.Next(decorators.Length - 1)];
		Console.CursorLeft = 0;
		while (true)
		{
			if (cooldown > 0 && symb_pos_x == -1) cooldown--;
			else if (cooldown == 0 && symb_pos_x == -1)
			{
				Console.CursorTop = random.Next(symb_top, 20 - 3);
				cooldown = random.Next(15, 40);
				symb = decorators[random.Next(decorators.Length - 1)];
				symb_pos_x = 0;
				symb_color = colors.GetRandom;
			}
			else if (symb_pos_x > -1)
			{
				Console.Write(new string(' ', symb_pos_x));
				if (symb_pos_x == Console.BufferWidth)
				{
					symb_pos_x = -1;
					Console.CursorTop -= 2;
				}
				else
				{
					Misc.Color.Print(symb.ToString(), symb_color, false);
					symb_pos_x++;
				}
			}
			if (Console.KeyAvailable)
			{
				answer = Console.ReadKey(true);
				if (((int)answer.Key >= 49 && (int)answer.Key <= 57 - (9 - variants.Length)) || (answer.Key == ConsoleKey.Escape && escape))
				{
					Console.Clear();
					if (answer.Key == ConsoleKey.Escape && escape) return -1;
					else return (int)answer.Key - 48;
				}
			}
			Thread.Sleep(20);
			Console.CursorLeft = 0;
			Console.CursorTop--;
			Console.Write(new string(' ', Console.BufferWidth));
		}
	}
	public static void Title(bool line)
	{
		Misc.Color.Print("	ASCIImation", ConsoleColor.Yellow, false);
		Misc.Color.Print($"    ver {Code.Version}", ConsoleColor.DarkGray, false);
		Console.CursorLeft = 50;
		Misc.Color.Print($"{Console.LargestWindowWidth}|{Console.LargestWindowHeight}", ConsoleColor.DarkBlue, line);
	}
	public static void Logo()
	{
		int timer = 0;
		while (true)
		{
			if (Console.KeyAvailable)
			{
				Console.ReadKey(true);
				break;
			}
			if (timer < 20)
			{
				Console.CursorTop = Console.BufferHeight - timer - 1;
				Console.CursorLeft = 0;
				Title(false);
			}
			else if (timer < 39)
			{
				Console.CursorTop = Console.BufferHeight * 2 - timer - 1;
				Console.CursorLeft = 0;
				Console.Write(new string(' ', Console.BufferWidth));
			}
			else break;
			timer++;
			Thread.Sleep(25);
		}
		Console.Clear();
	}
}

public class Colors
{
	public static Dictionary<ConsoleColor, char> ColorAccord = new Dictionary<ConsoleColor, char>
	{
		{ ConsoleColor.Black, '#' },
		{ ConsoleColor.Blue, '@' },
		{ ConsoleColor.Red, '$' },
		{ ConsoleColor.White, ' ' },
		{ ConsoleColor.Yellow, '%' },
		{ ConsoleColor.Magenta, '*' },
		{ ConsoleColor.Gray, '&' },
		{ ConsoleColor.Green, '~' },
		{ ConsoleColor.Cyan, '?' },
		{ ConsoleColor.DarkBlue, '=' },
		{ ConsoleColor.DarkCyan, '!' },
		{ ConsoleColor.DarkGreen, '-' },
		{ ConsoleColor.DarkMagenta, '^' },
		{ ConsoleColor.DarkRed, '«' },
		{ ConsoleColor.DarkYellow, '+' },
		{ ConsoleColor.DarkGray, '>' },
	};
	private ConsoleColor[] colors;
	public Colors(params ConsoleColor[] color_exceptions)
	{
		List<ConsoleColor> colorList = new List<ConsoleColor> { };
		foreach (ConsoleColor color in (ConsoleColor[])Enum.GetValues(typeof(ConsoleColor)))
			if (Array.IndexOf(color_exceptions, color) == -1)
				colorList.Add(color);
		this.colors = colorList.ToArray();
	}
	public Colors(bool _, params ConsoleColor[] color_target)
	{
		List<ConsoleColor> colorList = new List<ConsoleColor> { };
		foreach (ConsoleColor color in (ConsoleColor[])Enum.GetValues(typeof(ConsoleColor)))
			if (Array.IndexOf(color_target, color) != -1)
				colorList.Add(color);
		this.colors = colorList.ToArray();
	}
	public ConsoleColor this[int inx]
	{
		get => colors[inx];
		set => this.colors[inx] = value;
	}
	public ConsoleColor GetRandom
	{
		get => colors[new Random().Next(colors.Length - 1)];
	}
	public static ConsoleColor RGBtoConsoleColor(Color color)
	{
		ConsoleColor ret = 0;
		double rr = color.R;
		double gg = color.G;
		double bb = color.B;
		double delta = double.MaxValue;
		//if (color.A > 100) return Setup.Background;

		foreach (ConsoleColor cc in Enum.GetValues(typeof(ConsoleColor)))
		{
			var n = Enum.GetName(typeof(ConsoleColor), cc);
			var c = System.Drawing.Color.FromName(n == "DarkYellow" ? "Orange" : n);
			var t = Math.Pow(c.R - rr, 2.0) + Math.Pow(c.G - gg, 2.0) + Math.Pow(c.B - bb, 2.0);
			if (t == 0.0)
				return cc;
			if (t < delta)
			{
				delta = t;
				ret = cc;
			}
		}
		return ret;
	}
}


public static class ConsoleWork
{
	public static void Init()
	{
		Console.Title = "ASCIImation" + " v" + Code.Version;
		Console.CursorVisible = false;
		Console.WindowWidth = 70;
		Console.BufferWidth = 70;
		Console.WindowHeight = 20;
		Console.BufferHeight = 20;
	}
	public static void ClearLine()
	{
		Console.CursorLeft = 0;
		Console.Write(new string(' ', Console.BufferWidth - 1));
		Console.CursorLeft = 0;
	}
	public static void Pause(string message = "нажмите любую клавишу, чтобы продолжить", int left = 0, bool invert = false)
	{
		int timer = 0;
		Console.WriteLine(new string('\n', 2));
		while (true)
		{
			if (invert)
			{
				Console.BackgroundColor = Setup.Background;
				Console.ForegroundColor = ConsoleColor.Black;
			}
			if (timer == 0)
			{
				Console.CursorLeft = left;
				Console.Write(message);
			}
			else if (timer == 31)
			{
				Console.CursorLeft = left;
				Console.Write(new String(' ', Console.BufferWidth - (left + 1)));
			}
			if (timer >= 60) timer = 0;
			else timer++;
			if (Console.KeyAvailable)
			{
				Console.ResetColor();
				Console.ReadKey(true);
				Console.Clear();
				return;
			}
			Thread.Sleep(10);
		}
	}
	public static void Y(int y = 0) => Console.CursorLeft = y;
	public static void X(int x = 0) => Console.CursorLeft = x;
}

public struct Pixel
{
	public ConsoleColor color;
	public char symbol;
	public Pixel(ConsoleColor color)
	{
		this.color = color;
		this.symbol = Colors.ColorAccord[color];
	}
	public void Print()
	{
		Console.ForegroundColor = color;
		Console.Write(symbol + " ");
	}
	public void PrintBg()
	{
		Console.BackgroundColor = color;
		Console.Write("  ");
	}
}