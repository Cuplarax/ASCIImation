﻿using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;

public static class Code
{
	public const string Cancel = "$";  // Код, который будет возвращаться из некоторых функций, если пользователь выйдет из неё клавишой ESC.
	public static Version Version = new Version(1, 0);  // Версия программы.
	public static void Main(string[] args)
	{
		ConsoleWork.Init();
		//FileWork.GetBmpFiles(@"C:\Windows\System32");
		Misc.Logo();
		while (true)
		{
			Misc.Title(true);
			switch (Misc.Main_Menu(6, true, "загрузить кадры из папки", "проиграть анимацию", $"изменить задержку ({Setup.Delay} мс.)"))
			{
				case -1:
					Misc.Color.Print("вы хотите выйти?", ConsoleColor.Yellow);
					if (Misc.Main_Menu(4, true, "выйти", "отмена") == 1)
						return;
					break;
				case 2:
					Animation.Play();
					break;
				case 3:
					Misc.SetDelay(ref Setup.Delay);
					break;
				case 1:
					if (Misc.Choice(ref Setup.Path))
						FileWork.GetBmpFiles(Setup.Path);
					break;
			}
		}
	}
}
