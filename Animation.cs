﻿using System;
using System.Collections.Generic;
using System.Threading;


/// <summary>
/// статичческий класс, хранящий параметры.
/// </summary>
public static class Setup
{
	public static ConsoleColor Background = ConsoleColor.White;
	/// <summary>
	/// задержка между кадрами.
	/// </summary>
	public static int Delay = 100;
	/// <summary>
	/// папка, откуда будут браться кадры.
	/// </summary>
	public static string Path = @"C:\";
}

public static class Animation
{
	public static int FrameWidth = 0;
	public static int FrameHeight = 0;
	public static List<List<Pixel>> Frames = new List<List<Pixel>> { };
	public static void Play()
	{
		int cooldown = 0;
		int num = 0;
		int index = -1;
		if (Frames.Count > 0)
			if (FileWork.Selected) { }
			else
			{
				Misc.Color.Print("Последняя попытка загрузки кадров не удалась/nЗапустить последний удачный вариант?", ConsoleColor.Yellow);
				if (Misc.Main_Menu(5, true, "да", "нет") != 1)
					return;
			}
		else
		{
			FileWork.Error("кадров нет");
			return;
		}
		Console.BackgroundColor = Setup.Background;
		Console.Clear();
		if (!ConsoleSizing())
			return;
		while(true)
		{
			if (Console.KeyAvailable)
			{
				Console.ReadKey(true);
				break;
			}
			if (cooldown > 0)
			{
				cooldown--;
			}
			else
			{
				if (index < Frames.Count - 1)
				{
					cooldown = Setup.Delay;
					Console.CursorTop = 1;
					index++;
					num = 0;
					foreach (Pixel p in Frames[index])
					{
						num++;
						if (num == 1) Console.Write("      ");
						p.Print();
						if (num == FrameWidth)
						{
							num = 0;
							Console.WriteLine();
						}
					}
				}
				else
				{
					Console.ResetColor();
					Console.BackgroundColor = ConsoleColor.Black;
					ConsoleWork.Pause("нажмите любую клавишу, чтобы выйти в меню", 6, true);
					break;
				}
			}
			Thread.Sleep(1);
		}
		ConsoleWork.Init();
		Console.ResetColor();
		Console.Clear();
	}
	private static bool ConsoleSizing()
	{
		try
		{
			Console.BufferWidth = Console.WindowWidth = FrameWidth * 2 + 10;
			Console.BufferHeight = Console.WindowHeight = (int)(FrameHeight * 1.5) + 3;
			return true;
		}
		catch(EntryPointNotFoundException)
		{
			ConsoleWork.Init();
			FileWork.Error("кадр слишком большой для размера экрана.");
			return false;
		}
	}
}
